// 引入模块
import axios from 'axios'
import qs from 'qs'
import { Message } from 'element-ui'
// 自定义配置新建一个axios实例
const service = axios.create({
  baseURL: '/'
})
axios.withCredentials = true
axios.default.post['Content-Type'] = 'application/json; charset=UTF-8'
axios.default.timeout = 6000 * 5
axios.paramsSerializer = function (params) {
  return qs.stringify(params, { arrayFormat: 'brackets' })
}
// 添加请求拦截器
service.interceptors.request.use(
  config => {
    if (config.method === 'post') {
    // 序列化
      config.data = qs.stringify(config.data)
    }
    return Promise.resolve(config)
  },
  error => {
    return Promise.reject(error)
  }
)

// 响应拦截器
service.interceptors.response.use(
  response => {
    let status = response.status
    if (status === 200) {
      // eslint-disable-next-line eqeqeq
      if (response.data.code && response.data.code == 400) {
        localStorage.clear()
        location.href = '/#/login'
      } else if (!response.data.code) {
        return Promise.resolve(response)
      } else {
        Message({
          message: '服务器访问异常',
          type: 'warning',
          duration: 1.5 * 1000
        })
        return Promise.reject(response)
      }
    } else {
      Message({
        message: '服务器访问异常',
        type: 'warning',
        duration: 1.5 * 1000
      })
      return Promise.reject(response)
    }
  },
  error => {
    Message({
      message: '服务器访问异常',
      type: 'warning',
      duration: 1.5 * 1000
    })
    return Promise.reject(error)
  }
)
export default service
