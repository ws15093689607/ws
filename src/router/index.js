import Vue from 'vue'
import Router from 'vue-router'
// 底部导航
import Nav from '../views/Tab'
// 首页
import home from '../views/home'
// 品牌
import brand from '../views/brand'
// 购物车
import cart from '../views/cart'
import carts from '../components/cart/carts'
// 精选礼包详情
import goodsdetails from '../components/goodsDetail/goodsDetails'
// 确认订单
import Korder from '../components/order/Korder'
import selectShop from '../components/order/selectShop'
import payment from '../components/order/payment'
// 待付款
import orderDetail from '../components/order/orderDetail'
// 代发货
import ship from '../components/order/ship'
// 待收货
import Receipt from '../components/order/Receipt'
// 已完成
import complete from '../components/order/complete'
// 运输中
import transport from '../components/order/transport'
// 我的订单
import myorder from '../components/order/myorder'
// 撒码支付
import payCode from '../components/order/payCode'
// 我的
import mine from '../views/mine'
import integral from '../components/mine/integral'
import Balance from '../components/mine/Balance'
import withdraw from '../components/mine/withdraw'
import withdrawList from '../components/mine/withdrawList'
// 地址
// 地址管理
import runAddress from '../components/address/runAddress'
// 新增地址
import newAddress from '../components/address/newAddress'
// 编辑地址
import editAddress from '../components/address/editAddress'
// 登录，重置密码，注册
import login from '../views/login/login'
import setPwd from '../views/login/setPwd'
import register from '../views/login/register'
Vue.use(Router)

export default new Router({
  routes: [
    // 登录，重置密码，注册
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/setPwd',
      name: 'setPwd',
      component: setPwd
    },
    {
      path: '/register',
      name: 'register',
      component: register
    },
    // 底部导航
    {
      path: '/',
      name: 'Nav',
      component: Nav
    },
    // 首页
    {
      path: '/home',
      name: 'home',
      component: home
    },
    // 品牌
    {
      path: '/brand',
      name: 'brand',
      component: brand
    },
    // 购物车
    {
      path: '/cart',
      name: 'cart',
      component: cart
    },
    {
      path: '/',
      name: 'carts',
      component: carts
    },
    // 精选礼包详情
    {
      path: '/goodsdetails',
      name: 'goodsdetails',
      component: goodsdetails
    },
    // 确认订单
    {
      path: '/Korder',
      name: 'Korder',
      component: Korder
    },
    {
      path: '/selectShop',
      name: 'selectShop',
      component: selectShop
    },
    {
      path: '/payment',
      name: 'payment',
      component: payment
    },
    // 待付款
    {
      path: '/orderDetail',
      name: 'orderDetail',
      component: orderDetail
    },
    // 代发货
    {
      path: '/ship',
      name: 'ship',
      component: ship
    },
    // 待收货
    {
      path: '/Receipt',
      name: 'Receipt',
      component: Receipt
    },
    // 已完成
    {
      path: '/complete',
      name: 'complete',
      component: complete
    },
    // 运输中
    {
      path: '/transport',
      name: 'transport',
      component: transport
    },
    // 我的订单
    {
      path: '/myorder',
      name: 'myorder',
      component: myorder
    },
    // 扫码支付
    {
      path: '/payCode',
      name: 'payCode',
      component: payCode
    },
    // 我的
    {
      path: '/mine',
      name: 'mine',
      component: mine
    },
    {
      path: '/integral',
      name: 'integral',
      component: integral
    },
    {
      path: '/Balance',
      name: 'Balance',
      component: Balance
    },
    {
      path: '/withdraw',
      name: 'withdraw',
      component: withdraw
    },
    {
      path: '/withdrawList',
      name: 'withdrawList',
      component: withdrawList
    },
    // 地址管理
    {
      path: '/runAddress',
      name: 'runAddress',
      component: runAddress
    },
    // 新增地址
    {
      path: '/newAddress',
      name: 'newAddress',
      component: newAddress
    },
    // 编辑地址
    {
      path: '/editAddress',
      name: 'editAddress',
      component: editAddress
    }
  ]
})
